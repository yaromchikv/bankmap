package com.yaromchikv.bankmap.domain.usecase

import com.yaromchikv.bankmap.data.utils.Result
import com.yaromchikv.bankmap.domain.model.ATM
import com.yaromchikv.bankmap.domain.repository.BankRepository

class GetListOfATMsUseCase(private val repository: BankRepository) {
    suspend operator fun invoke(city: String): Result<List<ATM>> {
        return repository.getAtmList(city)
    }
}
