package com.yaromchikv.bankmap.domain.repository

import com.yaromchikv.bankmap.data.utils.Result
import com.yaromchikv.bankmap.domain.model.ATM

interface BankRepository {

    suspend fun getAtmList(city: String): Result<List<ATM>>
}