package com.yaromchikv.bankmap.domain.model

import com.squareup.moshi.Json

data class ATM(
    val id: String,
    @Json(name = "gps_x") val latitude: String,
    @Json(name = "gps_y") val longitude: String,
    @Json(name = "install_place") val place: String,
    @Json(name = "address_type") val addressType: String,
    @Json(name = "address") val address: String,
    @Json(name = "house") val house: String
)