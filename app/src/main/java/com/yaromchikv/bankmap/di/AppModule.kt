package com.yaromchikv.bankmap.di

import android.content.Context
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.yaromchikv.bankmap.data.api.BankApi
import com.yaromchikv.bankmap.data.repository.BankRepositoryImpl
import com.yaromchikv.bankmap.domain.repository.BankRepository
import com.yaromchikv.bankmap.domain.usecase.GetListOfATMsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private const val BASE_URL = "https://belarusbank.by/"

    private val moshi = MoshiConverterFactory.create(
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    )

    @Provides
    @Singleton
    fun provideBankApi(): BankApi = Retrofit.Builder()
        .addConverterFactory(moshi)
        .baseUrl(BASE_URL)
        .build()
        .create(BankApi::class.java)

    @Provides
    @Singleton
    fun provideBankRepository(@ApplicationContext context: Context, api: BankApi): BankRepository =
        BankRepositoryImpl(context, api)

    @Provides
    @Singleton
    fun provideGetListOfATMsUseCase(repository: BankRepository): GetListOfATMsUseCase =
        GetListOfATMsUseCase(repository)
}