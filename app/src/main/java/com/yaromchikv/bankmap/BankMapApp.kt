package com.yaromchikv.bankmap

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BankMapApp : Application()
