package com.yaromchikv.bankmap.data.api

import com.yaromchikv.bankmap.domain.model.ATM
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface BankApi {

    @GET("api/atm")
    suspend fun getListOfATMs(
        @Query("city") city: String
    ): Response<List<ATM>>
}