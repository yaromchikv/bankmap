package com.yaromchikv.bankmap.data.repository

import android.content.Context
import com.yaromchikv.bankmap.R
import com.yaromchikv.bankmap.data.api.BankApi
import com.yaromchikv.bankmap.data.utils.Result
import com.yaromchikv.bankmap.domain.model.ATM
import com.yaromchikv.bankmap.domain.repository.BankRepository

class BankRepositoryImpl(
    private val context: Context,
    private val api: BankApi,
) : BankRepository {

    override suspend fun getAtmList(city: String): Result<List<ATM>> {
        return try {
            val response = api.getListOfATMs(city)
            val result = response.body()
            if (response.isSuccessful && result != null)
                Result.Success(result)
            else
                Result.Error(response.message())
        } catch (e: Exception) {
            Result.Error(context.getString(R.string.connection_error))
        }
    }

}