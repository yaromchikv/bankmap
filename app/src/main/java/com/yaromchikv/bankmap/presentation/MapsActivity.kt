package com.yaromchikv.bankmap.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.yaromchikv.bankmap.R
import com.yaromchikv.bankmap.databinding.ActivityMapsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class MapsActivity : AppCompatActivity(R.layout.activity_maps), OnMapReadyCallback {

    private val binding by viewBinding(ActivityMapsBinding::bind)
    private val viewModel by viewModels<MapsViewModel>()

    private var map: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        setupCollector()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        val gomel = LatLng(52.431227, 30.992708)
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, 10f))
    }

    private fun setupCollector() {
        lifecycleScope.launchWhenStarted {
            viewModel.listOfATMs.collectLatest {
                when (it) {
                    is MapsViewModel.UiState.Ready -> {
                        if (map != null && it.data != null) {
                            it.data.forEach { atm ->
                                addMarkers(
                                    position = LatLng(
                                        atm.latitude.toDouble(),
                                        atm.longitude.toDouble()
                                    ),
                                    title = atm.place,
                                    snippet = getString(
                                        R.string.adress,
                                        atm.addressType,
                                        atm.address,
                                        atm.house
                                    ),
                                )
                            }
                        }
                    }
                    is MapsViewModel.UiState.Error -> {
                        Toast.makeText(this@MapsActivity, it.error, Toast.LENGTH_LONG).show()
                    }
                    else -> Unit
                }
            }
        }
    }

    private fun addMarkers(position: LatLng, title: String, snippet: String) {
        map?.addMarker(
            MarkerOptions()
                .icon(getMarkerIcon())
                .position(position)
                .title(title)
                .snippet(snippet)
        )
    }

    private fun getMarkerIcon(): BitmapDescriptor {
        val bitmap = AppCompatResources.getDrawable(this, R.drawable.ic_marker)?.toBitmap()
        return if (bitmap != null)
            BitmapDescriptorFactory.fromBitmap(bitmap)
        else
            BitmapDescriptorFactory.defaultMarker()
    }
}