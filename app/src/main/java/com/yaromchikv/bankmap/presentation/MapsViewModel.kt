package com.yaromchikv.bankmap.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.yaromchikv.bankmap.data.utils.Result
import com.yaromchikv.bankmap.domain.model.ATM
import com.yaromchikv.bankmap.domain.usecase.GetListOfATMsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class MapsViewModel @Inject constructor(
    private val getListOfATMsUseCase: GetListOfATMsUseCase
) : ViewModel() {

    private val _listOfATMs = MutableStateFlow<UiState>(UiState.Idle)
    val listOfATMs = _listOfATMs.asStateFlow()

    init {
        viewModelScope.launch {
            val result = getListOfATMsUseCase("Гомель")
            _listOfATMs.value = when (result) {
                is Result.Success -> UiState.Ready(result.data)
                is Result.Error -> UiState.Error(result.message)
            }
        }
    }

    sealed class UiState {
        class Ready(val data: List<ATM>?) : UiState()
        class Error(val error: String?) : UiState()
        object Idle : UiState()
    }
}